defmodule Diskuy.Repo do
  use Ecto.Repo,
    otp_app: :diskuy,
    adapter: Ecto.Adapters.Postgres
end
