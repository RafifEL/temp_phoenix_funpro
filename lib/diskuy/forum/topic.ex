defmodule Diskuy.Forum.Topic do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  schema "topics" do
    field :name, :string
    timestamps()
  end

  @doc false
  def changeset(topic, attrs) do
    topic
    |> cast(attrs, [:id_topic, :name])
    |> validate_required([:id_topic, :name])
    |> unique_constraint(:name)
  end
end
