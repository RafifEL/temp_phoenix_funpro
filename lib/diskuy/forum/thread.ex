defmodule Diskuy.Forum.Thread do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  schema "threads" do
    field :points, :integer
    field :title, :string
    field :topic_id, :binary_id
    field :user_id, :string

    timestamps()
  end

  @doc false
  def changeset(thread, attrs) do
    thread
    |> cast(attrs, [:id, :title, :points])
    |> validate_required([:id, :title, :points])
    |> unique_constraint(:title_in_topic_unique, name: :title_in_topic_unique)
  end
end
