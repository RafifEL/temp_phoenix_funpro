defmodule Diskuy.Forum.Post do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  schema "posts" do
    field :message, :string
    field :points, :integer
    field :status, :string
    field :thread_id, :binary_id
    field :post_parent_id, :binary_id
    field :user_id, :string

    timestamps()
  end

  @doc false
  def changeset(post, attrs) do
    post
    |> cast(attrs, [:message, :points])
    |> validate_required([:message, :points])
  end
end
