defmodule DiskuyWeb.PostController do
  use DiskuyWeb, :controller

  alias Diskuy.Forum
  alias Diskuy.Forum.Post

  action_fallback DiskuyWeb.FallbackController

  def index(conn, _params) do
    posts = Forum.list_posts()
    render(conn, "index.json", posts: posts)
  end

  def create(conn, %{"post" => post_params}) do
    with {:ok, %Post{} = post} <- Forum.create_post(post_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.post_path(conn, :show, post))
      |> render("show.json", post: post)
    end
  end

  def show(conn, %{"id" => id}) do
    post = Forum.get_post!(id)
    render(conn, "show.json", post: post)
  end

  def update(conn, %{"id" => id, "post" => post_params}) do
    post = Forum.get_post!(id)

    with {:ok, %Post{} = post} <- Forum.update_post(post, post_params) do
      render(conn, "show.json", post: post)
    end
  end

  def delete(conn, %{"id" => id}) do
    post = Forum.get_post!(id)

    with {:ok, %Post{}} <- Forum.delete_post(post) do
      send_resp(conn, :no_content, "")
    end
  end
end
