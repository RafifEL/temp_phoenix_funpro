defmodule DiskuyWeb.PostView do
  use DiskuyWeb, :view
  alias DiskuyWeb.PostView

  def render("index.json", %{posts: posts}) do
    %{data: render_many(posts, PostView, "post.json")}
  end

  def render("show.json", %{post: post}) do
    %{data: render_one(post, PostView, "post.json")}
  end

  def render("post.json", %{post: post}) do
    %{id: post.id,
      message: post.message,
      points: post.points,
      status: post.status,
      user_id: post.user_id,
      post_parent_id: post.post_parent_id,
      thread_id: post.thread_id
    }
  end
end
