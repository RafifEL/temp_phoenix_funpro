defmodule DiskuyWeb.ThreadView do
  use DiskuyWeb, :view
  alias DiskuyWeb.ThreadView

  def render("index.json", %{threads: threads}) do
    %{data: render_many(threads, ThreadView, "thread.json")}
  end

  def render("show.json", %{thread: thread}) do
    %{data: render_one(thread, ThreadView, "thread.json")}
  end

  def render("thread.json", %{thread: thread}) do
    %{id: thread.id,
      title: thread.title,
      points: thread.points,
      topic_id: thread.topic_id,
      user_id: thread.user_id
    }
  end
end
