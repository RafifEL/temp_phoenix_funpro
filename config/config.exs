# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :diskuy,
  ecto_repos: [Diskuy.Repo]

# Configures the endpoint
config :diskuy, DiskuyWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "2vHhQp+z9WMYDBV5qTBRAul6jdLAlDYdFZVOaCZ0vYQSFJdkj5A/9dBRIDpe5tIv",
  render_errors: [view: DiskuyWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: Diskuy.PubSub,
  live_view: [signing_salt: "aKmTxBag"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
