# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Diskuy.Repo.insert!(%Diskuy.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias Diskuy.Repo
alias Diskuy.Account.User
alias Diskuy.Forum.Topic
alias Diskuy.Forum.Thread


# Repo.insert!(%User{
#   username: "rafif",
#   id: "rafif@gmail.com"
# })

# Repo.insert!(%Topic{
#   name: "Gaming",
# })

Repo.insert!(%Thread{
  title: "Klee Gaming",
  points: 0,
  topic_id: "86284ef4-a22e-4c4b-8e46-212fa8a7a3f3",
  user_id: "rafif@gmail.com"
})
