defmodule Diskuy.Repo.Migrations.CreateThreads do
  use Ecto.Migration

  def change do
    create table(:threads, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :title, :string
      add :points, :integer
      add :topic_id, references(:topics, on_delete: :nothing, type: :uuid)
      add :user_id, references(:users, on_delete: :nothing, type: :string)

      timestamps()
    end

    create index(:threads, [:topic_id])
    create index(:threads, [:user_id])
    create unique_index(:threads, [:title, :topic_id], name: :title_in_topic_unique)
  end
end
