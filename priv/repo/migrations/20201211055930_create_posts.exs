defmodule Diskuy.Repo.Migrations.CreatePosts do
  use Ecto.Migration

  def change do
    create table(:posts, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :message, :text
      add :points, :integer
      add :status, :string
      add :thread_id, references(:topics, on_delete: :nothing, type: :uuid)
      add :post_parent_id, references(:posts, on_delete: :nothing, type: :uuid)
      add :user_id, references(:users, on_delete: :nothing, type: :string)

      timestamps()
    end

    create index(:posts, [:thread_id])
    create index(:posts, [:post_parent_id])
    create index(:posts, [:user_id])
  end
end
