defmodule Diskuy.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users, primary_key: false) do
      add :username, :string
      add :id, :string, primary_key: true

      timestamps()
    end
    create unique_index(:users, [:username])
  end
end
